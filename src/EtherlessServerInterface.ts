/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {EthereumConnectionConfigurationInterface} from "./smart/EthereumConnectionConfigurationInterface";
import {PackagedSoftware} from "./PackagedSoftware";
import {SoftwareExecutionRequest} from "./SoftwareExecutionRequest";
import {SoftwareExecutionResult} from "./SoftwareExecutionResult";
import {TokenInterface} from "./TokenInterface";
import {EthereumAccountInterface} from "./smart/EthereumAccountInterface";

import {DerivedKeyInterface} from "./DerivedKeyInterface";

export interface EtherlessServerInterface {

    init(outdatedConfiguration: EthereumConnectionConfigurationInterface, address:string, key: DerivedKeyInterface): Promise<EthereumConnectionConfigurationInterface>;

    requestToken(address:string, key: DerivedKeyInterface): Promise<TokenInterface>;

    deploy(software: PackagedSoftware, token: TokenInterface, address:string, key: DerivedKeyInterface): Promise<void>;

    run(request: SoftwareExecutionRequest, token: TokenInterface, address:string, key: DerivedKeyInterface): Promise<SoftwareExecutionResult>;

    update(software: PackagedSoftware, token: TokenInterface, address:string, key: DerivedKeyInterface): Promise<void>;

    remove(softwareName: string, token: TokenInterface, address:string, key: DerivedKeyInterface): Promise<void>;

    log(token: TokenInterface, address:string, key: DerivedKeyInterface): Promise<SoftwareExecutionResult>;
    
}