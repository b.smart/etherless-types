/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
    EtherlessSoftwareDeployOperation,
    EtherlessSoftwareUpgradeOperation,
    EtherlessSoftwareRemovalOperation,
    EtherlessSoftwareExecutionOperation
} from "./EtherlessSoftwareOperation";
import {EthereumConnectionConfigurationInterface} from "./EthereumConnectionConfigurationInterface";
import {TokenInterface} from "../TokenInterface";
import {SoftwareDetails} from "../SoftwareDetails";
import {EthereumAmount} from "./EthereumAmount";

/*
    This interface represents the Etherless smart contract and possible interactions.
*/
export interface EtherlessSmartReadOnlyInterface {

    readonly connectionConfiguration: EthereumConnectionConfigurationInterface;

    getBlockNumber(): Promise<number>;

    getAddress(): Promise<string>;

    softwareExists(name: string): Promise<boolean>;

    getSoftwareDetails(name: string): Promise<SoftwareDetails>;

    getFirstSoftwarePublicationEvent(
        from: string|undefined,
        name: string|undefined,
        token: TokenInterface|undefined,
        timeout: number|undefined
    ): Promise<EtherlessSoftwareDeployOperation>;

    getFirstSoftwareUpdateEvent(
        from: string|undefined,
        name: string|undefined,
        token: TokenInterface|undefined,
        timeout: number|undefined
    ): Promise<EtherlessSoftwareUpgradeOperation>;

    getFirstSoftwareRemovalEvent(
        from: string|undefined,
        name: string|undefined,
        token: TokenInterface|undefined,
        timeout: number|undefined
    ): Promise<EtherlessSoftwareRemovalOperation>;

    getFirstSoftwareExecutionEvent(
        from: string|undefined,
        name: string|undefined,
        token: TokenInterface|undefined,
        timeout: number|undefined
    ): Promise<EtherlessSoftwareExecutionOperation>;

    getAllSoftwareExecutionByAddress(from: string): Promise<Array<EtherlessSoftwareExecutionOperation>>;

    getSoftwareList(): Promise<Array<Promise<SoftwareDetails>>>;

    getBalance(address: string): Promise<EthereumAmount>;
}