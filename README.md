# etherless-types

This contains the core type definitions to be used across etherless-smart, etherless-server and etherless-cli.

## Build
Build using

```sh
npm run build
```